
## @css[title-red](でーたばいんでぃんぐのえほん)

### @css[description](Akane Yamashita / 2018.02.18(Tue))

---

## @css[title](Agenda)

### @css[description](AndroidArchitectureに欠かせないデータバインディングについて、精神年齢を下げめで紹介します🐶)


#### - でーたばいんでぃんぐってなあに？
#### - でーたばいんでぃんぐのおともだち
#### - はたらくでーたばいんでぃんぐ
#### - でーたばいんでぃんぐのおべんきょう
---

## @css[title](でーたばいんでぃんぐってなあに？)

## @css[title](DOM操作(どむそうさ))
### @css[description](Javascriptではhtmlをユーザーが操作するイベント毎に処理を書く)
<div class="image-inline">
![MVVM](onclick.png)
</div>

---

## @css[title](AndroidのおともだちであるMVVMで同じことをしたい)
### @css[description](😏)

---

### @css[description](はろーでーたばいんでぃんぐ！)
![Hello](hello.png)

---
- xmlファイルを元に紐ついたDatabindingクラスをビルドする
![Databinding](bindingclass.png)
---

- Viewに紐ついたイベントをリスナーを設定したクラスで呼び出せてとても便利
<div class="image-inline">
![ChatDB](chatbindingclass.png)
</div>
---

![ChatViewflow](express.png)
---
## @css[title](でーたばいんでぃんぐのおともだち)


### @css[description](- 単方向データバイディング)
### @css[description](- 双方向データバイディング)

<div class="image-inline">
![DB](db_double.png)
</div>
---


## @css[title](はたらくでーたばいんでぃんぐ)
### @css[title](ViewModelとの併用)
### @css[description](ViewObjectと値変更の挙動を紐つけられる)
![observer](observer_plus)

---

## @css[title-green](Android Studio)
### @css[description](-> xml, style files or drawable)

<div class="image-inline">
![XMLICON](https://cloud.netlifyusercontent.com/assets/344dbf88-fdf9-42bb-adb4-46f01eedd629/674a517a-5330-4524-a6b2-9b6d4fb1475f/tab-bar-cons-1-preview-opt.png)
</div>
---

### @css[title-red](Rails can use ...)


![FORM_FOR](https://i.stack.imgur.com/RgtoA.png)
---

### form_for method
```slim
= form_for(@user) do |f|
  = f.label :uid do
    = f.input :uid
  = f.label :password do
    = f.input :password
  = f.label :rember_me do
    = f.check_box :remember_me
  = f.label "Change password" do 
    = f.check_box :change_passwd
  = f.submit "Log in"
```
---
### @css[title-green](Android can use ...)

![EDIT_TXT](https://i.stack.imgur.com/ypsXX.png)
---

### Drawing by xml
```xml
      <com.google.android.material.textfield.TextInputLayout
              android:layout_width="match_parent"
              android:layout_height="wrap_content"
              android:textColorHint="@color/colorPrimaryText" >
        <com.google.android.material.textfield.TextInputEditText
		android:id="@+id/user_email" 
		android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:hint="@string/user_email"/>
      </com.google.android.material.textfield.TextInputLayout>
      <com.google.android.material.textfield.TextInputLayout
              android:layout_width="match_parent"
              android:layout_height="wrap_content">
        <com.google.android.material.textfield.TextInputEditText
                android:id="@+id/user_password"
		android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:hint="@string/user_password"/>
      </com.google.android.material.textfield.TextInputLayout>

    <Button
            android:id="@+id/signin_btn"
            android:text="@string/signin_btn_text"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/colorMedically"/>
```
---

## Copy & Paste 😇
---
## @css[title](でーたばいんでぃんぐのおべんきょう)

## @css[title-red](Rails)
### @css[description](-> Gemfile)
![Gemfile](assets/img/Gemfile.PNG)

---

## @css[title-green](Android Studio)
### @css[description](-> Gradle)

---

## Thank you for listening🤗
